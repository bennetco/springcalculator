<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="calculator.*"%>
 
<jsp:useBean id="calculator" class="calculator.Calculator" scope="request" />
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
    <head>
        <title>Corey Bennett - Simple Calculator App</title>
    </head>
 
    <body>
    	<br>
		<jsp:getProperty property="expression" name="calculator"/> <!-- The expression currently in the calculator -->
		<br>
		
		<!-- Create the buttons for the calculator -->
		<form method="post" action="calculator.html">  
		<%! String s; %>
		<% for(int i=0;i<3;i++) {  %>  
			<% for(int j=0;j<5;j++) { %>
				<% if(j<3){ s = Integer.toString(7 + j - 3*i); %>
					<input type="submit" value=<%=s %> name=<%=s %>>
				<% } else { %>
					<% if(i==0) { s = (j==3)?"+":"M+"; %>
						<input type="submit" value=<%=s %> name=<%=s %>>
					<% } else if(i==1) { s = (j==3)?"-":"MR"; %>
						<input type="submit" value=<%=s %> name=<%=s %>>
					<% } else { s = (j==3)?"*":"MC"; %>
						<input type="submit" value=<%=s %> name=<%=s %>>
					<% } %>
				<% } %>
			<% } %>
			<br>
		<% } %>
		<input type="submit" value="." name="." />
		<input type="submit" value="0" name="0"/>
    	<input type="submit" value="=" name="="/>
    	<input type="submit" value="/" name="/"/>
    	<input type="submit" value="C" name="C"/>
		</form>
     </body>
 </html>