package calculator;
 
import java.io.Serializable;

import org.springframework.stereotype.Component;

import bsh.Interpreter;

/**
 * Business logic for a calculator
 * @author Corey Bennett
 */
@Component
public class Calculator implements Serializable {

	private static final long serialVersionUID = 1L;
	private String expression;
	private transient double result;
	private double memory;
	
	/**
	 * Constructors
	 */
    public Calculator() {
    	this("0", 0);
    }
 
    public Calculator(String expression) {
        this(expression, 0);
    }
    
    public Calculator(String expression, double memory) {
    	this.expression = expression;
    	this.result = 0;
    	this.memory = memory;
    	if(!expression.equals("")) this.evaluate(); //avoid eval error
    }
    
    /**
     * Validate input and perform appropriate actions on data
     */
    public void parseAction(String action) {
    	if(expression.equals("Syntax Error")) setExpression("0");
    	switch(action){
    	case "-":
    		if(expression.equals("")) {
    			concat(action);
    			break;
    		}
    		if(expression == "0" || expression == "0.0") {
    			setExpression(action);
    			break;
    		}
    	case "+":
    	case "*":
    	case "/":
    		if(expression.endsWith(action)) break;
    		if(((String) "+-*/").indexOf(expression.charAt(expression.length()-1)) >= 0) //don't want multiple operators in a row
    			expression = expression.substring(0, expression.length()-1);
    		concat(action);
    		break;
    	case ".":
    		if(expression.endsWith(action)) break;
    		concat(action);
    		break;
    	case "C":
    		clear();
    		break;
    	case "=":
    		evaluate();
    		break;
    	case "M+":
    		if(!evaluate()) break;
    		memory += getResult();
    		break;
    	case "MC":
    		memory = 0;
    		break;
    	case "MR":
    		setResult(memory);
    		setExpression(Double.toString(memory));
    		break;
    	default:
    		if(expression == "0" || expression == "0.0") expression = action;
    		else concat(action);
    		break;
    	}
    }
    
    /**
     * Adds on the String to the current expression.  Validation of the input is handled in parseAction().
     * @param s String to add to expression
     */
    public void concat(String s) {
    	expression += s; 
    }
    
    /**
     * This is just one of many, many possible ways to actually do the calculations;
     * I used BeanShell's Interpreter, found on StackOverflow and available thru Maven, primarily for time concerns.
     * @return whether the evaluation was successful
     */
    public boolean evaluate() {	
    	Interpreter i = new Interpreter();
    	try {
			i.eval("val = (double)" + getExpression());
			setExpression(i.get("val").toString());
			result = Double.parseDouble(getExpression());
			return true;
		} catch (Throwable e) {
			expression = "Syntax Error";
			return false;
		}	
    }
    
    /**
     * Clear expression and result values
     */
    public void clear(){
    	result = 0;
    	expression = "";
    }
    
    /**
     * Accessors and mutators
     */
    public double getResult() { 
    	return result; 
    }
    
    public void setResult(double d){
    	result = d;
    }

	public String getExpression() {
		return expression;
	}
	
	public void setExpression(String ex) {
		expression = ex;
	}
	
    @Override
    public String toString() {
        return memory + ":" + getExpression();
    }
}