package calculator;
 
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
 
/**
 * Controller for Calculator in MVC
 * @author Corey Bennett
 */
@Controller
public class CalculatorController {
 
    @Autowired
    private Calculator calculator;
 
    @RequestMapping(value="/calculator")
    public ModelAndView calculator(HttpServletRequest request){
    	//Retrieve the parameter from the request (a bit roundabout but necessary with so many submit inputs)
        Enumeration<?> e = request.getParameterNames();
        if(e.hasMoreElements())
        	calculator.parseAction(e.nextElement().toString());
 
        //Pass back view & model
        return new ModelAndView("calculator.jsp", "calculator", calculator);
    }
}